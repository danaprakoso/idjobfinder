<?php

class Employer extends CI_Controller {
	
	public function get_jobs() {
		$employerID = $this->input->post('employer_id');
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$employerID = $this->input->post('employer_id');
		$jobs = $this->db->query("SELECT * FROM `jobs` WHERE `employer_id`=" . $employerID . " ORDER BY `date` DESC LIMIT " . $start . "," . $length)->result_array();
		echo json_encode($jobs);
	}
	
	public function add_job() {
		$employerID = $this->input->post('employer_id');
		$title = $this->input->post('title');
		$description = $this->input->post('description');
		$salary = $this->input->post('salary');
		$processingTime = $this->input->post('processing_time');
		$processingTimeUnit = $this->input->post('processing_time_unit');
		$address = $this->input->post('address');
		$date = $this->input->post('date');
		$this->db->insert('jobs', array(
			'employer_id' => intval($employerID),
			'title' => $title,
			'description' => $description,
			'salary' => intval($salary),
			'processing_time' => intval($processingTime),
			'processing_time_unit' => $processingTimeUnit,
			'address' => $address,
			'date' => $date
		));
	}
	
	public function update_job() {
		$id = $this->input->post('id');
		$employerID = $this->input->post('employer_id');
		$title = $this->input->post('title');
		$description = $this->input->post('description');
		$salary = $this->input->post('salary');
		$processingTime = $this->input->post('processing_time');
		$processingTimeUnit = $this->input->post('processing_time_unit');
		$address = $this->input->post('address');
		$date = $this->input->post('date');
		$this->db->where('id', $id);
		$this->db->update('jobs', array(
			'employer_id' => intval($employerID),
			'title' => $title,
			'description' => $description,
			'salary' => intval($salary),
			'processing_time' => intval($processingTime),
			'processing_time_unit' => $processingTimeUnit,
			'address' => $address,
			'date' => $date
		));
	}
	
	public function delete_job() {
		$id = $this->input->post('id');
		$this->db->query("DELETE FROM `jobs` WHERE `id`=" . $id);
		$this->db->query("DELETE FROM `applications` WHERE `job_id`=" . $id);
	}
	
	public function get_applications() {
		$employerID = $this->input->post('employer_id');
		$start = intval($this->input->post('start'));
		$length = intval($this->input->post('length'));
		$jobs = $this->db->query("SELECT * FROM `applications` WHERE `employer_id`=" . $employerID . " ORDER BY `date` DESC LIMIT " . $start . "," . $length)->result_array();
		for ($i=0; $i<sizeof($jobs); $i++) {
			$jobs[$i]['job'] = $this->db->query("SELECT * FROM `jobs` WHERE `id`=" . $jobs[$i]['job_id'])->row_array();
			$jobs[$i]['employer'] = $this->db->query("SELECT * FROM `users` WHERE `id`=" . $jobs[$i]['employer_id'])->row_array();
		}
		echo json_encode($jobs);
	}
	
	public function approve_job() {
		$id = $this->input->post('id');
		$this->db->query("UPDATE `applications` SET `status`='approved' WHERE `id`=" . $id);
		$application = $this->db->query("SELECT * FROM `applications` WHERE `id`=" . $id)->row_array();
		$user = $this->db->query("SELECT * FROM `users` WHERE `id`=" . $application['user_id'])->row_array();
		$job = $this->db->query("SELECT * FROM `jobs` WHERE `id`=" . $application['job_id'])->row_array();
		$title = "";
		if ($status == 'approved') {
			$title = "Selamat, lamaran Anda sebagai " . $job['title'] . " disetujui";
		} else if ($status == 'rejected') {
			$title = "Lamaran Anda sebagai " . $job['title'] . " ditolak";
		}
		FCM::send_notification($title, "Klik untuk mengecek status terbaru lamaran Anda", $user['fcm_id'], array());
	}
	
	public function reject_job() {
		$id = $this->input->post('id');
		$reason = $this->input->post('reason');
		$this->db->query("UPDATE `applications` SET `status`='rejected', `reject_reason`='" . $reason . "' WHERE `id`=" . $id);
		$application = $this->db->query("SELECT * FROM `applications` WHERE `id`=" . $id)->row_array();
		$user = $this->db->query("SELECT * FROM `users` WHERE `id`=" . $application['user_id'])->row_array();
		$job = $this->db->query("SELECT * FROM `jobs` WHERE `id`=" . $application['job_id'])->row_array();
		$title = "";
		if ($status == 'approved') {
			$title = "Selamat, lamaran Anda sebagai " . $job['title'] . " disetujui";
		} else if ($status == 'rejected') {
			$title = "Lamaran Anda sebagai " . $job['title'] . " ditolak";
		}
		FCM::send_notification($title, "Klik untuk mengecek status terbaru lamaran Anda", $user['fcm_id'], array());
	}
}
