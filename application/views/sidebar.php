<?php
?>
<div class="nav-wrapper">
	<ul class="nav flex-column">
		<li class="nav-item">
			<a class="<?php echo ($current_menu == 'admin')?'nav-link active':'nav-link' ?>" href="http://idjobfinder.xyz/adminpanel/admin">
				<i class="material-icons">admin_panel_settings</i>
				<span>Admin</span>
			</a>
		</li>
		<li class="nav-item">
			<a class="<?php echo ($current_menu == 'user')?'nav-link active':'nav-link' ?>" href="http://idjobfinder.xyz/adminpanel/user">
				<i class="material-icons">person</i>
				<span>Pengguna</span>
			</a>
		</li>
		<li class="nav-item">
			<a class="<?php echo ($current_menu == 'job')?'nav-link active':'nav-link' ?>" href="http://idjobfinder.xyz/adminpanel/job">
				<i class="material-icons">work</i>
				<span>Pekerjaan</span>
			</a>
		</li>
		<li class="nav-item">
			<a class="<?php echo ($current_menu == 'application')?'nav-link active':'nav-link' ?>" href="http://idjobfinder.xyz/adminpanel/applicant">
				<i class="material-icons">cloud_done</i>
				<span>Lamaran</span>
			</a>
		</li>
		<li class="nav-item">
			<a class="<?php echo ($current_menu == 'logout')?'nav-link active':'nav-link' ?>" href="http://idjobfinder.xyz/adminpanel/logout">
				<i class="material-icons">logout</i>
				<span>Keluar</span>
			</a>
		</li>
	</ul>
</div>
