const PROTOCOL = "http";
const HOST = "idjobfinder.xyz/adminpanel";
const API_URL = PROTOCOL+"://"+HOST;
const USERDATA_URL = PROTOCOL+"://"+HOST+"/userdata/";

$(document).ready(function() {
});

function logout() {
	$.redirect('http://idjobfinder.xyz/adminpanel/logout');
}

function formatMoney(amount) {
	return amount.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
}
